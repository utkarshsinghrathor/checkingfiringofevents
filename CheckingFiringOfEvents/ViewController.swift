//
//  ViewController.swift
//  CheckingFiringOfEvents
//
//  Created by Mac-6 on 07/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{

    @IBOutlet var tblView: newTableView! ;
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell : UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell") ;
        
        cell.textLabel!.text = String(indexPath.row  ) + " Row" ;
        
        cell.contentView.backgroundColor = UIColor.yellowColor() ;
        
        return cell ;
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 40 ;
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header : UIView = UIView(frame: CGRectMake(0,0,100,60)) ;
        
        let label : UILabel = UILabel(frame: CGRectMake(0,0,100,60));
        
        label.text = "UTKARSH" ;
        
        header.addSubview(label) ;
        
        
        header.backgroundColor = UIColor.purpleColor() ;
        return header ;
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let first : firstView = firstView() ;
        
        let second : secondView = secondView() ;
        
        let third : thirdView = thirdView() ;
        
        let fourth : fourthView = fourthView() ;
        
    
        
        
        first.frame = CGRectMake(0, 0, 200, 200) ;
        
        second.frame = CGRectMake(0, 0, 100 , 100) ;
        
        third.frame = CGRectMake(0, 0,50,50 ) ;
        
        fourth.frame  = CGRectMake(100,400,50, 50) ;
        
        
        
        
        
        second.addSubview(third) ;
        first.addSubview(second) ;
        self.view.addSubview(fourth) ;
        self.view.addSubview(first) ;
        
        
        
        first.backgroundColor = UIColor.blueColor() ;
        second.backgroundColor = UIColor.redColor() ;
        third.backgroundColor = UIColor.yellowColor() ;
        fourth.backgroundColor = UIColor.purpleColor() ;
        
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?)
    {
        print("\n\n\n ViewController calls the function in which " + __FUNCTION__ + " called\n\n\n") ;
        
        super.traitCollectionDidChange(previousTraitCollection) ;
        
        
        
    }

}

