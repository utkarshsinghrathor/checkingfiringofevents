//
//  firstView.swift
//  CheckingFiringOfEvents
//
//  Created by Mac-6 on 07/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation
import UIKit

class firstView : UIView
{
    var name: String = "first" ;
    
    override var description : String
    {
        print("Calling teh description function for the application");
        
        return  (self.name ?? "noname")
    }
    
    override func updateConstraints()
    {
        super.updateConstraints()
        
        print("\(self)\n\(__FUNCTION__)\n")
    }
    
    // gets an extra cycle, I've no idea why
    override func layoutSublayersOfLayer(layer: CALayer)
    {
        super.layoutSublayersOfLayer(layer)
        
        print("\(self)\n\(__FUNCTION__)\n")
        
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        print("\(self)\n\(__FUNCTION__)\n")
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?)
    {
        // super.traitCollectionDidChange(previousTraitCollection)
        print("\(self)\n\(__FUNCTION__)\n")
        //        let prev : UITraitCollection? = previousTraitCollection
        //        if prev == nil {
        //            print("nil")
        //        }
        //        let none = "none"
        //        print("old: \(previousTraitCollection ?? none)\n")
    }
    
    override class func layerClass() -> AnyClass
    {
        return MyLoggingLayer.self
    }
}

class MyLoggingLayer : CALayer
{
    override func layoutSublayers()
    {
        super.layoutSublayers()
        guard let del = self.delegate else {return}
        print("layer of \(del)\n\(__FUNCTION__)\n")
    }
}
