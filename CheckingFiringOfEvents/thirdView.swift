//
//  firstView.swift
//  CheckingFiringOfEvents
//
//  Created by Mac-6 on 07/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation
import UIKit

class thirdView : UIView
{
    var name: String = "third" ;
    
    override var description : String
        {
            print("Calling teh description function for the application");
            
            return (self.name ?? "noname")
    }
    
    override func updateConstraints()
    {
        super.updateConstraints()
        
        print("\(self)\n\(__FUNCTION__)\n")
    }
    
    // gets an extra cycle, I've no idea why
    override func layoutSublayersOfLayer(layer: CALayer)
    {
        super.layoutSublayersOfLayer(layer)
        
        print("\(self)\n\(__FUNCTION__)\n")
        
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        print("\(self)\n\(__FUNCTION__)\n")
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?)
    {
        // super.traitCollectionDidChange(previousTraitCollection)
        print("\(self)\n\(__FUNCTION__)\n")
        //        let prev : UITraitCollection? = previousTraitCollection
        //        if prev == nil {
        //            print("nil")
        //        }
        //        let none = "none"
        //        print("old: \(previousTraitCollection ?? none)\n")
    }
    
    override class func layerClass() -> AnyClass
    {
        return MyLoggingLayer.self
    }
}

